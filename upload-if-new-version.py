#!/usr/bin/python3

import json
import urllib.request
import subprocess
import toml


PACKAGE_NAME = "sapui5-xml-linter"

response = urllib.request.urlopen(url=f"https://pypi.org/pypi/{PACKAGE_NAME}/json")
body_str = "\n".join([line.decode() for line in response.readlines()])
body = json.loads(body_str)

current_version = body["info"]["version"]

with open("pyproject.toml") as file_handle:
    project_configuration = toml.load(file_handle)

new_version = project_configuration["project"]["version"]

if new_version == current_version:
    print("INFO:", "not deploying, since no version bump")
else:
    print("INFO:", "deploying, since version has changed")
    subprocess.run("twine upload dist/*", shell=True)
