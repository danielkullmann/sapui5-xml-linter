.PHONY: build clean upload bump-patch justbump-patch bump-minor justbump-minor bump-major justbump-major

build:
	python3 -m build

check:
	pylint setup.py sapui5-xml-linter

clean:
	rm -rf dist/ build/ sapui5_xml_linter.egg-info/

upload: clean check build
	twine upload -u danielkullmann dist/*

justbump-patch:
	bump2version patch

bump-patch: justbump-patch upload

justbump-minor:
	bump2version minor

bump-minor: justbump-minor upload

justbump-major:
	bump2version major

bump-major: justbump-major upload

